"""
Copyright 2016-2018 Imagunet S.A.S or its affilites. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License").

AWS Lambda process to filter out relevant notifications for early alert system

"""
import json
import requests
import urllib3
import os
import boto3
import time
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from boto3.dynamodb.conditions import Attr

print('Loading function')

""" Main Function """
def lambda_handler(event, context):
    for record in event['Records']:
        if record['eventName']=='INSERT':
            # Lambda function triggered by Dynamo insert events. Relevant fields are processed below
            GPIOSensor={}
            GPIOSensor['Sensor_Name'] = record['dynamodb']['NewImage']['Sensor_NAME']['S']
            GPIOSensor['TimeStamp'] = record['dynamodb']['NewImage']['TimeStamp']['S']
            GPIOSensor['Humidity'] = record['dynamodb']['NewImage']['Humidity']['S']
            GPIOSensor['Latitude'] = record['dynamodb']['NewImage']['Latitude']['S']
            GPIOSensor['Longitude'] = record['dynamodb']['NewImage']['Longitude']['S']
            query_coordinates = GPIOSensor['Latitude']+","+GPIOSensor['Longitude']
            
            #The following function uses the sensor coordinates to return a postal code
            zip_code = zip_code_query(query_coordinates)
            
            #The postal code is used to retrieve the demographic information of the region of interest through the following function
            demographics = demographics_query(zip_code)
            
            for item in demographics['response']['result']['package']['item']:
                dynamodb = boto3.resource('dynamodb')
                live_sensor_status_table = dynamodb.Table('DDB_NaturalDisaster_LiveSensorStatus')
                
                response = live_sensor_status_table.put_item(
                    Item={
                        'SensorName':GPIOSensor['Sensor_Name'],
                        'ZipCode':zip_code,
                        'Pop_Age_00_04':item["age00_04"],
                        'Pop_Age_05_09':item["age05_09"],
                        'Pop_Age_10_14':item["age10_14"],
                        'Pop_Age_15_19':item["age15_19"],
                        'CountyName':item["countyname"],
                        'StateName':item["statename"],
                        'ClosestCity':item["city500_closest_name"],
                        'ClosestAirport':item['airport'],
                        'AirportDist':item['airportdist'],
                        'Humidity':record['dynamodb']['NewImage']['Humidity']['S'],
                        'TimeToLive':600 + int(time.time())
                    }
                )
                
                age1="age00_04"
                age2="age05_09"
                age3="age10_14"
                age4="age15_19"
                
                print('Received record')
                print(record)
                
                #The following condition checks if the sensor's humidity threshold has been reached. If so, the alert issueing process is triggered.
                if record['dynamodb']['NewImage']['Humidity']['S']=='True':
                    #The following function creates or updates the index in the Elasticsearch service.
                    indexing_population(zip_code, age1, item[age1], age2, item[age2], age3, item[age3], age4, item[age4], query_coordinates, item["countyname"], item["statename"], item["city500_closest_name"], item["airport"], item["airportdist"], GPIOSensor['Latitude'],GPIOSensor['Longitude'],GPIOSensor['Sensor_Name'])
                    message_body="\n\t\tA natural disaster risk alert has been issued by sensor "+GPIOSensor['Sensor_Name']+" because its humidity threshold has been reached. This sensor is located in postal code "+zip_code+", in "+item["countyname"]+", "+item["statename"]+". People in ages 0 to 19 are considered to have a higher risk of becoming victims of human trafficking. "
                    message_body+="High-risk population is distributed as follows: \n\n Ages 0 to 4:\t\t"+item[age1]+"\n Ages 5 to 9:\t\t"+item[age2]+"\n Ages 10 to 14:\t\t"+item[age3]+"\n Ages 15 to 19:\t\t"+item[age4]+"\n\n\t\tThe closest large city is "+item["city500_closest_name"]+". The closest airport is "+item["airport"]+", which is "+item["airportdist"]+" miles away. "
                    message_body+="Visit the following URL for more details: \n\nhttps://search-iot-naturaldisastermonitor-ubw3td3lxykzauhd2ejn3d7t6e.us-east-1.es.amazonaws.com/_plugin/kibana/app/kibana#/dashboard/b5d42f20-cb89-11e8-b935-b992266551b3?_g=h@3fa6f60&_a=h@0910adf \n\nRegards,\n\nNatural Disaster Early Alert System"
                    #An e-mail alert is triggered using the SNS service in the following function:
                    send_email("High risk of natural disaster alert", message_body)
                else:
                    print('Starting Cleanup')
                    #verify if there are any sensors on this zipcode turned on
                    responseQuery = live_sensor_status_table.scan(
                        FilterExpression=Attr('Humidity').eq('True') & Attr('ZipCode').eq(zip_code)
                    )
                    print(responseQuery)
                    if(responseQuery['Count']==0):
                        #proceed to eliminate index from population
                        remove_index_population(zip_code)
                
    return 'Successfully processed {} records.'.format(len(event['Records']))
	
#The following functiona uses the coordinates of the sensor to query its postal code. It returns the postal code.
def zip_code_query (coordinates):
    # settings
    BASE_URL = "http://dev.virtualearth.net/REST/v1/Locations/"
    key="AjsfZc6VExVG1MIQIUmLD9O8qYfewjepkxQ4MGI-Kzs3afoGbO83fGY9DtQ5wCre"
    
    #The HTTP-formated query is put together here
    returned_data = requests.get(BASE_URL+str(coordinates)+"?includeEntityTypes=Postcode1&includeNeighborhood=0&key="+str(key)).json()

    #The results returned by the Geocode API, are put together to match the format required by the Places API.
    for resourceSet in returned_data['resourceSets']:
        for resource in resourceSet['resources']:
            zip_code=resource['address']['postalCode']

    return zip_code

#The following function uses a postal code as input to return its demographics information as result.
def demographics_query(zip_code):
    api_url = os.environ['ATTOM_API_URL']
    api_key = os.environ['ATTOM_API_KEY']
    api_headers = {'accept':'application/json','apikey':api_key}
    demographics_result = requests.get(api_url+"?AreaId=ZI"+zip_code,headers=api_headers).json()
    return demographics_result

#The following function verifies if an index exists, if it doesn't it creates it, and then populates its fields with relevant data.    
def indexing_population(zip_code, age1, pop1, age2, pop2, age3, pop3, age4, pop4, coordinates, county, state, city, airport, airportdist, latitude, longitude, sensor_name):
    es = Elasticsearch(['https://search-iot-naturaldisastermonitor-ubw3td3lxykzauhd2ejn3d7t6e.us-east-1.es.amazonaws.com'])
    
    #es.indices.delete("population")

    if not es.indices.exists(index="population"):
        document = {
            "mappings":{
                "_doc": {
                    "properties": {
                        "location": {
                            "type": "geo_point"
                        }
                    }
                }
            }
        }
        es.indices.create(index='population', body=document)
    es_entries = {
        'sensor_name' : str(sensor_name),
        "zip_code": zip_code,
		age1: int(pop1),
		age2: int(pop2),
		age3: int(pop3),
		age4: int(pop4),
		"county":county,
		"state":state,
		"closest_city":city,
		"closest_airport":airport,
		"airport_dist":airportdist,
		"danger": "True",
        'location': str(latitude)+","+str(longitude)
    }
    es.index(index="population", doc_type="_doc", body=es_entries, id=zip_code)
    #es.indices.delete("population")
    #es.indices.delete("sensorgeodata")
    return "Ok"

#The following function sends an e-mail with contents that have been previously arranged and passed through. 
def send_email(subject,body):
    sns = boto3.client('sns')
    topic=os.environ['SNS_TOPIC']
    sns.publish(
        TopicArn=topic,
        Subject=subject,
        Message=body
        )

def remove_index_population(zip_code):
    es = Elasticsearch(['https://search-iot-naturaldisastermonitor-ubw3td3lxykzauhd2ejn3d7t6e.us-east-1.es.amazonaws.com'])
    response = es.delete(index="population",doc_type="_doc",id=zip_code, ignore=[400, 404])
    print(response)
      
#def index_cleanup():
    #print("IndexCleanup")
    #es = Elasticsearch(['https://search-iot-naturaldisastermonitor-ubw3td3lxykzauhd2ejn3d7t6e.us-east-1.es.amazonaws.com'])
    #es.indices.delete("population")