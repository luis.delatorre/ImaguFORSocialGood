import json
import re
import os
from math import sin, cos, sqrt, atan2, radians
import time
import datetime

import subprocess

# must create a file object to store the output. Here we are getting
# the ssid we are connected to

ts=time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d,%H:%M:%S')

cmd="sudo iwlist scan|egrep \"Addr|level\"|head -8"
lines = subprocess.check_output(cmd, shell=True).splitlines()
i=0
for i in range(0,7):
  lines[i]=str(lines[i])
  i=i+1

apjson=open("/home/pi/Location/ap.json","w")
apjson.write('{\"considerIp\": \"false\",\"wifiAccessPoints\": [')
i=0
for i in range(0,5,2):
    apjson.write('{\"macAddress\": \"'+lines[i][lines[i].find('Address: ')+9:lines[i].rfind('\n')]+'\",\"signalStrength\": '+lines[i+1][lines[i+1].find('level=')+6:lines[i+1].rfind('dBm')]+'}')
    if i<4:
      apjson.write(',')
apjson.write(']}')
apjson.close()

urlgoogle="curl -d @/home/pi/Location/ap.json -H \"Content-Type: application/json\" -i \"https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAkS_FpXSrmrb2HDO4YElYmjE8RbpANQ5E\""




resp=str(subprocess.check_output(urlgoogle, shell=True))
#print(resp)
subresp='{'+resp[resp.find('lat')-1:resp.find('}')].replace('\\n','')+'}'
print(subresp)
newLocInfo = json.loads(subresp)
print ('Latitude: ' + str(newLocInfo['lat']))
print ('Longitude: ' + str(newLocInfo['lng']))
oldposition=open("/home/pi/Location/position.txt","r")
lastLocInfo=json.loads(str(oldposition.read()))
print ('Latitude: ' + str(lastLocInfo['lat']))
print ('Longitude: ' + str(lastLocInfo['lng']))
oldposition.close()


R = 6373000

lat1 = radians(lastLocInfo['lat'])
lon1 = radians(lastLocInfo['lng'])
lat2 = radians(newLocInfo['lat'])
lon2 = radians(newLocInfo['lng'])

dlon = lon2 - lon1
dlat = lat2 - lat1

a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
c = 2 * atan2(sqrt(a), sqrt(1 - a))

distance = R * c
oldposition=open("/home/pi/Location/position.log","a")
oldposition.write(str(st)+',' + str(newLocInfo['lat']) + ',' + str(newLocInfo['lng']) + '\n')
oldposition.close()
if distance > 3:
    oldposition=open("/home/pi/Location/position.txt","w")
    oldposition.write('{\"lat\": ' + str(newLocInfo['lat']) + ',\"lng\": ' + str(newLocInfo['lng']) + '}')
    oldposition.close()    
    print("Se Movio")
print("Result:", distance)
