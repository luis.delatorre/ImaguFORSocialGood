Imagu Disaster Alert tool

Description
Imagunet tool for connecting Soil Mositure sensors via RaspberryPI running AWS IOT SDK to a set of AWS services for storing (DynamoDB), distributing (Lambda), generating notifications (SNS) and pushing dashboards (ElasticSearch+Kibana).
The core Lambda function triggers API calls to enrich the alert with demographic information about vulnerable population in the affected perimeter which helps to protect people from potential human traffickers.

Configuration
Each folder contains one stage of the complete workflow


CollectionEngine - Python service to collect data from Soil moisture sensors connected to GPIO ports in a RaspberryPI calibrated for sensibility via external potentiometer and ADCs. All information is passed as a JSON to the MQTT publish functions of the AWS IOT SDK.

LocationEngine - Python service to collect geo-positioning based on Gateway measurement of signal strenght (via WIFI or Cellular networks) which feeds into the CollectionService.

Imagu-NaturalDisasterMonitor - Lambda function triggered after MQTT data is pushed into the DynamoDB table called DDB_GPIOSensor. It performs validation of Soil Humidaity values and triggers API calls for demographics of pre-defined perimeter areas if a potential disaster is indentified.
