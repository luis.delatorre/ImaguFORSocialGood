'''
/*
 * Copyright 2016-2018 Imagunet S.A.S or its affilites. All Rights Reserved.
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
 '''

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from configparser import SafeConfigParser
import os
import sys
import time
import logging
import datetime
import argparse
import json
import RPi.GPIO as GPIO

# Custom MQTT message callback

# Read in command-line parameters
parser = argparse.ArgumentParser()
parser.add_argument("-e", "--endpoint", action="store", required=True, dest="host", help="Your AWS IoT custom endpoint")
parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
parser.add_argument("-c", "--cert", action="store", dest="certificatePath", help="Certificate file path")
parser.add_argument("-k", "--key", action="store", dest="privateKeyPath", help="Private key file path")
parser.add_argument("-w", "--websocket", action="store_true", dest="useWebsocket", default=False,
                    help="Use MQTT over WebSocket")
parser.add_argument("-id", "--clientId", action="store", dest="clientId", default="CollectionEngine v1.3", help="Targeted client id")
parser.add_argument("-t", "--topic", action="store", dest="topic", default="sdk/test/Python", help="Targeted topic")

args = parser.parse_args()
host = args.host
rootCAPath = args.rootCAPath
certificatePath = args.certificatePath
privateKeyPath = args.privateKeyPath
useWebsocket = args.useWebsocket
clientId = args.clientId
topic = args.topic

if args.useWebsocket and args.certificatePath and args.privateKeyPath:
	parser.error("X.509 cert authentication and WebSocket are mutual exclusive. Please pick one.")
	exit(2)

if not args.useWebsocket and (not args.certificatePath or not args.privateKeyPath):
	parser.error("Missing credentials for authentication.")
	exit(2)

# Configure logging
logging.basicConfig(level=logging.DEBUG,filename='/home/pi/Collection/logs/CollectMQTT.log',format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#logger = logging.getLogger("AWSIoTPythonSDK.core")
#logger.setLevel(logging.NOTSET)
#streamHandler = logging.FileHandler('/home/pi/Collection/logs/CollectMQTT.log')
#formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#streamHandler.setFormatter(formatter)
#logger.addHandler(streamHandler)

# Read Configuration files
conf_parser=SafeConfigParser()
conf_parser.read('/home/pi/Collection/AuthMQTT.ini')
DrainingFrequency=conf_parser.get('MQTT','DrainingFrequency')
MQTTPublishFrequency=conf_parser.getfloat('MQTT','MQTTPublishFrequency')
MQTTOperationTimeout=conf_parser.get('MQTT','MQTTOperationTimeout')
MQTTConnectDisconnectTimeout=conf_parser.get('MQTT','MQTTConnectDisconnectTimeout')
MQTTKeepAliveFrequency=conf_parser.get('MQTT','MQTTKeepAliveFrequency')
SensorTimeout=conf_parser.get('GPIO','timeout')

# Init AWSIoTMQTTClient
print("\n  ... \n")
myAWSIoTMQTTClient = None
if useWebsocket:
	myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId, useWebsocket=True)
	myAWSIoTMQTTClient.configureEndpoint(host, 443)
	myAWSIoTMQTTClient.configureCredentials(rootCAPath)
else:
	myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
	myAWSIoTMQTTClient.configureEndpoint(host, 8883)
	myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(DrainingFrequency)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(MQTTConnectDisconnectTimeout)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(MQTTOperationTimeout)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect(int(MQTTKeepAliveFrequency))
print("Connected to AWS IOT")
#myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
time.sleep(2)


# Get active Sensor data on predefined GPIO ports
init = False

GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme

def get_status(pin = 8):
    GPIO.setup(pin, GPIO.IN) 
    return GPIO.input(pin)

def build_message(delay, water_sensor_pin):
    print("Here we go! Press CTRL+C to exit")
    try:
        while True:
            time.sleep(delay)
            wet_data = get_status(pin = water_sensor_pin) == 0
            logging.info("Payload of message being sent: %s\n" % wet_data)
            # Generate timestamp
            ts = time.time()
            ts_ttl = ts+7*24*3600
            tms = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
            # Obtain last location from Location.py in /home/pi/Location
            location_raw = open("/home/pi/Location/position.txt","r")
            location_serial = json.loads(location_raw.read())
            # Build JSON payload for MQTT publish
            data = {}
            data['Sensor_NAME'] = topic
            data['TimeStamp'] = tms 
            data['Humidity'] = str(wet_data)
            data['Latitude'] = str(location_serial['lat'])
            data['Longitude'] = str(location_serial['lng'])
            data['Time_To_Live'] = int(ts_ttl)
            wet_data_json = json.dumps(data)
            logging.info("JSON payload sent: %s\n" % wet_data_json)
            myAWSIoTMQTTClient.publish(topic, wet_data_json, 1)
    except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
        GPIO.cleanup() # cleanup all GPI

#Getting measurements with defined Delay (field 1) and overwrite GPIO port (field 2)
build_message(int(SensorTimeout),7)
