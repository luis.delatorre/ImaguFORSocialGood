#!/bin/sh
# run CollectionEngine app using certificates downloaded in package

printf "\r\nRunning CollectionEngine application\r\n"

HWaddr=`ifconfig | grep HWaddr | grep wlan0 | tr -d ":" | awk '{print $5}' | tr 'a-z' 'A-Z'`

python3 /home/pi/Collection/CollectionEngine.py -e a1rkuvc8q3xd1z.iot.us-east-1.amazonaws.com -r /home/pi/AWS/root-CA.crt -c /home/pi/AWS/IOTG-Rasp-b827eb05f83c.cert.pem -k /home/pi/AWS/IOTG-Rasp-b827eb05f83c.private.key -t GPIOSensor/$HWaddr
